# $ZDOTDIR/.zshrc
# Sourced in interactive shells. 
# Should contain commands to set up aliases, functions, options, keybindings, etc.

# Set this to 1 to enable (very slow) profiling of .zshrc as it's sourced
# logged to /tmp/startlog.*
PROFILE_ZSHRC=0
# Set this to 1 to enable internal reporting of how long zshrc takes to source.
TIME_ZSHRC=0
if [[ $TIME_ZSHRC -eq 1 ]]; then
	start_time=$(( $(( $(date +%s) * 1000000000 )) + $(date +%N) ))
fi
if [[ $PROFILE_ZSHRC -eq 1 ]]; then
	setopt prompt_subst
	PS4='$(date "+%s,%N"),%N,%i, '
	exec 3>&2 2>/tmp/startlog.$$
	setopt xtrace 
fi

# Declare external plugins zundle can install and update.
# Load zundle before setting zundle_bundles (otherwise is set to empty list)
# Do NOT put zundle into this list or it will stomp itself
source "$HOME/.zsh/bundle/zundle/rc"
zundle_bundles=(
	zsh-users/zsh-completions
	zsh-users/zsh-syntax-highlighting
	zsh-users/zsh-history-substring-search
)

# Allow autoload from files in ~/.zsh/autoload,
# mark each one to be loaded at first use
# like aliases, these should be defined in every new shell
fpath=( "$HOME/.zsh/autoload" "${fpath[@]}" )
autoload_scripts=(~/.zsh/autoload/*)
for autoload_script in $autoload_scripts; do
	#echo "${autoload_script:t}"
	autoload -Uz ${autoload_script:t}
done
unset autoload_script; unset autoload_scripts

# Source files which contain hooks or keybindings (not aliases/functions).
# - portable aliases should be in ~/.alias
# - zsh functions should be in ~/.zsh/autoload; these shouldn't, because
#   they may never be called by name and must be sourced eagerly.
# - these may change fpath, but should not run compinit
# - stuff which doesn't need to be defined in every new shell (e.g., many
#   environment variables) can often be put in ~/.profile or similar.
autoload -Uz add-zsh-hook
hook_scripts=(~/.zsh/hooks/*.zsh)
for hook_script in $hook_scripts; do 
	[ -e "$hook_script" ] && source $hook_script
done 
unset hook_scripts; unset hook_script

# Source single file for loose aliases/functions; anything which isn't
# a one-liner should move to autoload eventually.
# the same file is also sourced by bash automatically - keep it portable and
# you can use either freely, with your aliases.
# aliases need to be defined in every new shell, so zshrc is appropriate
[ -e ~/.alias ] && source ~/.alias

# Forcibly load complist before zundle runs compinit,
# AND before the menuselect keymap may be used etc.
zmodload zsh/complist

# Ask terminals to enable 256 colors and not clobber Ctrl-S.
# this should happen before keybindings.zsh so it can know what keys to use
[ -e ~/.fix_terminal ] && source ~/.fix_terminal

# zsh-specific settings modules
filenames=(options keybindings prompt completion)
for filename in "$HOME/.zsh/"${^filenames}".zsh"; do
	source $filename
done
unset filename{,s}

# Set fpath to include bundles, run compinit, source rc files
# Hmmm... this is really slow... maybe later
#time ( LoadBundles )

# Since zundle is not running compinit for now, just do it manually
autoload -U compinit
compinit -u

# === Stuff below here can happen any time and can be moved to plugins. ===

if [ "$TERM" != "dumb" ]; then
	[ -e ~/.zsh/bundle/zsh-syntax-highlighting/zsh-syntax-highlighting.zsh ] && \
		source ~/.zsh/bundle/zsh-syntax-highlighting/zsh-syntax-highlighting.zsh
else
	unset LESSOPEN
	unset LESSCLOSE
fi
[ -e ~/.zsh/bundle/zsh-history-substring-search/zsh-history-substring-search.zsh ] && \
	source ~/.zsh/bundle/zsh-history-substring-search/zsh-history-substring-search.zsh

# Compile .zcompdump for speedup - but compiling other things helps less
autoload zrecompile
zrecompile "$HOME/.zcompdump"

# The End.
if [[ $TIME_ZSHRC -eq 1 ]]; then
	end_time=$(( $(( $(date +%s) * 1000000000 )) + $(date +%N) ))
	duration=$(( $end_time - $start_time ))
	echo "startup took roughly $(( $duration / 1000000.0 ))ms"
	unset TIME_ZSHRC; unset start_time; unset end_time; unset duration
fi
if [[ $PROFILE_ZSHRC -eq 1 ]]; then
	unsetopt xtrace
	exec 2>&3 3>&-
	unset PROFILE_ZSHRC
fi

if (( $+commands[vex] )) ; then
	eval "$(vex --shell-config zsh)"
fi

[ -f ~/.fzf.zsh ] && source ~/.fzf.zsh
