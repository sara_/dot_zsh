# Coarse distro detection
release=`cat /etc/*-release`
if [[ "$release" =~ ".*Ubuntu.*" ]]; then
	distro="ubuntu"
elif [[ "$release" =~ ".*Debian.*" ]]; then
	distro="debian"
elif [[ "$release" =~ ".*Fedora.*" ]]; then
	distro="fedora"
elif "$release" =~ ".*Red Hat.*" ]]; then
	distro="redhat"
fi

# Set lazy-man update alias depending on distro
rpm_distros=(redhat fedora)
deb_distros=(debian ubuntu)
if [[ -n ${deb_distros[(r)$distro]} ]]; then
	alias upd='sudo apt-get update && sudo apt-get upgrade -y'
elif [[ -n ${rpm_distros[(r)$distro]} ]]; then
    alias upd='sudo yum update -y'
fi

